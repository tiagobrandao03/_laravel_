<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title') - LARAVEL</title>
    </head>
    <body>
        <header>
            <h1>Header</h1>
        </header>
        <hr>
        <main>
            @yield('content')
        </main>
        <hr>
        <footer>
            @yield('footer')
        </footer>
    </body>
</html>