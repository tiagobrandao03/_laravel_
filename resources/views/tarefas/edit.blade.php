@extends('layouts.admin')
@section('title','Edição de Tarefas')
@section('content')
    <h1>Edição</h1>

    @if(session('Warning'))
        @alert
        {{session('warning')}}
        @endalert

    @endif
    {{-- @php
    var_dump('Warning')    
    @endphp --}}
    
        <form method="POST">
            @csrf
        <label for="">
            Titulo <br>
            <input type="text" name="titulo" name="{{$data->titulo}}">
        </label>
        <input type="submit" value="Salvar">
    </form>
@endsection